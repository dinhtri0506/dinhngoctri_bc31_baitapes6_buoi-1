let headingEl = document.querySelector(".heading");
let text = headingEl.textContent;
let textCharacter = [...text];

let jumpText = () => {
  let contentHTML = "";
  for (let i = 0; i < textCharacter.length; i++) {
    let character = textCharacter[i];
    contentHTML += `<span>${character}</span>`;
  }
  headingEl.innerHTML = contentHTML;
};
jumpText();
