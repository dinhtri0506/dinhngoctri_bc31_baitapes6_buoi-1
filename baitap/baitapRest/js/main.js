let tinhDTB = (resultEl, ...rest) => {
  let total = 0;
  for (let i = 0; i < rest.length; i++) {
    total += rest[i];
  }
  let DTB = total / rest.length;
  resultEl.innerHTML = DTB.toFixed(2);
};

let DOM = (...rest) => {
  let elementArray = [];
  for (let i = 0; i < rest.length; i++) {
    let element = document.querySelector(rest[i]);
    elementArray.push(element);
  }
  return elementArray;
};

let elementArray = DOM(
  "#inpToan",
  "#inpLy",
  "#inpHoa",
  "#inpVan",
  "#inpSu",
  "#inpDia",
  "#inpEnglish",
  "#tbKhoi1",
  "#btnKhoi1",
  "#tbKhoi2",
  "#btnKhoi2"
);

let tinhDTB_khoiLop1 = () => {
  tinhDTB(
    elementArray[7],
    elementArray[0].value * 1,
    elementArray[1].value * 1,
    elementArray[2].value * 1
  );
};
elementArray[8].addEventListener("click", tinhDTB_khoiLop1);

let tinhDTB_khoiLop2 = () => {
  tinhDTB(
    elementArray[9],
    elementArray[3].value * 1,
    elementArray[4].value * 1,
    elementArray[5].value * 1,
    elementArray[6].value * 1
  );
};
elementArray[10].addEventListener("click", tinhDTB_khoiLop2);
