const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let change_color = (color) => {
  let house = document.getElementById("house");
  house.className = "house " + color;
};

let render_color_button = () => {
  let buttonList = "";
  for (let i = 0; i < colorList.length; i++) {
    let color = colorList[i];
    let button = "";
    if (i == 0) {
      button = ` <button class="color-button ${color} active"></button>`;
    } else {
      button = ` <button class="color-button ${color}"></button>`;
    }
    buttonList += button;
  }
  document.getElementById("colorContainer").innerHTML = buttonList;
  let buttonDOM = document.querySelectorAll("#colorContainer .color-button");
  for (let i = 0; i < colorList.length; i++) {
    let color = colorList[i];
    buttonDOM[i].addEventListener("click", function () {
      /** không dùng arrow function, vì arrow function không định nghĩa this */
      change_color(color);
      for (let i = 0; i < colorList.length; i++) {
        buttonDOM[i].classList.remove("active");
        this.classList.add("active");
      }
    });
  }
};
render_color_button();
